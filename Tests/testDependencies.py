#! /usr/bin/env python

# Configure path to find modules to test
import sys
import os.path
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

import unittest
from ApeTools import Build
from ApeTools import Config
Tree = Build.DependencyTree


class P(Build.Package):
    def __init__(self, name, dependencies=[], weakDependencies=[]):
        if not Config.config.has_section("package " + name):
            Config.config.add_section("package " + name)
        self.dependencies = dependencies
        self.weakDependencies = weakDependencies
        Build.Package.__init__(self, name)

    def __repr__(self):
        return "<P instance: %s>" % self.name


class testDependencies(unittest.TestCase):
    def setUp(self):
        if Config.config is None:
            Config.init()
        Config.config.set("package", "version", "0")
        Config.config.set("package", "prefix", "p")
        Config.config.set("package", "buildDirectory", "b")
        Config.config.set("package", "sourceDirectory", "b")
        Config.config.set("package", "builder", "Package")
        Config.config.set("ape", "preDependencies", "")
        self.p1 = P("P1")
        self.p2 = P("P2", [self.p1])
        self.p3 = P("P3", [self.p1])
        self.p4 = P("P4", [self.p1, self.p1])
        self.c0 = P("C0")
        self.c0.dependencies = [self.c0]
        self.c1 = P("C1")
        self.c2 = P("C2", [self.c1])
        self.c1.dependencies = [self.c2]
        self.w1 = P("W1")
        self.w2 = P("W2")
        self.w3 = P("W3", [self.w1], [self.w2])
        self.w4 = P("W4", [], [self.w1])
        self.w5 = P("W5", [self.w1], [self.w4])
        self.w6 = P("W6", [], [self.w5])
        self.w7 = P("W7", [self.w1], [self.w1, self.w4, self.w5])

    def testNone(self):
        t = Tree([self.p1])
        self.assertEqual(t.buildList(), [self.p1])

    def testOne(self):
        t = Tree([self.p2])
        self.assertEqual(t.buildList(), [self.p1, self.p2])

    def testOrdered(self):
        t = Tree([self.p2, self.p1])
        self.assertEqual(t.buildList(), [self.p1, self.p2])

    def testRepeated(self):
        t = Tree([self.p4])
        self.assertEqual(t.buildList(), [self.p1, self.p4])

    def testTwo(self):
        t = Tree([self.p2, self.p3])
        self.assertEqual(t.buildList()[0], self.p1, self.p2)
        self.assertEqual(len(t.buildList()), 3)

    def testCircular1(self):
        self.assertRaises(Build.CircularDependencyError, Tree, [self.c0])

    def testCircular2(self):
        self.assertRaises(Build.CircularDependencyError, Tree, [self.c1])
        self.assertRaises(Build.CircularDependencyError, Tree, [self.c2])

    def testWeakOrdering1(self):
        t = Tree([self.w3])
        self.assertEqual(t.buildList(), [self.w1, self.w3])

    def testWeakOrdering2(self):
        t = Tree([self.w2, self.w3])
        self.assertEqual(t.buildList()[2], self.w3)

    def testWeakOrdering3(self):
        t = Tree([self.w3, self.w2])
        self.assertEqual(t.buildList()[2], self.w3)

    def testWeakOrderingOrder1(self):
        t = Tree([self.w4, self.w5])
        self.assertEqual(t.buildList(), [self.w1, self.w4, self.w5])

    def testWeakOrderingOrder2(self):
        t = Tree([self.w5, self.w4])
        self.assertEqual(t.buildList(), [self.w1, self.w4, self.w5])

    def testWeakOrderingOrderRepetition1(self):
        t = Tree([self.w7, self.w1])
        self.assertEqual(t.buildList(), [self.w1, self.w7])

    def testWeakOrderingOrderRepetition2(self):
        t = Tree([self.w7, self.w4, self.w1])
        self.assertEqual(t.buildList(), [self.w1, self.w4, self.w7])

    def testWeakPropagates(self):
        t = Tree([self.w6])
        self.assertEqual(t.buildList(), [self.w6])

    def testExplictOverridesWeak(self):
        t = Tree([self.w6, self.w5])
        self.assertEqual(t.buildList(), [self.w1, self.w5, self.w6])

    def testDependants(self):
        self.assertEqual(self.w1.dependants(), set([self.w3, self.w4, self.w5, self.w7]))
        self.assertEqual(self.w2.dependants(), set([self.w3]))

if __name__ == '__main__':
    unittest.main()

#! /usr/bin/env python

# Configure path to find modules to test
import sys
import os.path
import os
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

import unittest
import optparse
from ApeTools import Fetch
from ApeTools import Config

Config.init()
mirrors = "us mx si"


class testFetch(unittest.TestCase):
    def setUp(self):
        self.parser = optparse.OptionParser()
        Config.addOptions(self.parser)
        Config.init()

    def testReadMirrors(self):
        (options, args) = self.parser.parse_args(["--mirrors=us:mx:si"])
        Config.init(options)

        manager = Fetch.DownloadManager()
        self.assertEqual(manager.mirrorList[0].tag, "us")
        self.assertEqual(manager.mirrorList[0].location,
                         "Northeastern University, Boston, USA")
        self.assertEqual(manager.mirrorList[0].url,
                         "http://129.10.132.228/apeDist")
        self.assertEqual(manager.mirrorList[0].user, "beyond")

        self.assertEqual(manager.mirrorList[1].tag, "mx")
        self.assertEqual(manager.mirrorList[2].tag, "si")

    def testOrderMirrors(self):
        (options, args) = self.parser.parse_args(["--mirrors=mx"])
        Config.init(options)

        manager = Fetch.DownloadManager()
        self.assertEqual(manager.mirrorList[0].tag, "mx")

    def testSha1(self):
        """Checksums generated in Tests directory running

        .. code-block:: sh

           openssl sha1 run test.rc > SHA1

        followed by a change in the line of test.rc to create a checksum
        missmatch for that file.
        """
        Config.init()
        Config.config.set("ape internal", "sha1File", "%(apedir)s/Tests/SHA1")

        manager = Fetch.DownloadManager()
        self.assertEqual(manager.checkSha1("some_unknown_file.ext"), None)
        cwd = os.getcwd()
        os.chdir(os.path.join(Config.apeDir, "Tests"))
        self.assert_(manager.checkSha1("run"))
        self.assert_(not manager.checkSha1("test.rc"))
        os.chdir(cwd)

if __name__ == '__main__':
    unittest.main()

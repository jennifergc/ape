#! /usr/bin/env python

"""
Sub-Module :mod:`Version`
=========================

This module provides the version information of ``ape``. It is used in python
code and, as a tool, in Makefiles.

It provides two variables with the version and release number of ``ape``.

If run as a script, it prints :data:`fullVersion`. If run with the option
`--short`,  it prints :data:`version`.

.. data:: version

   The ape major and minor version, without release number.

   :type: string, without spaces.

.. data:: fullVersion

   The full ``ape`` version, including patch number and possible
   dev/alpha/beta/rc suffixes. It has to start with :data:`version`.

   :type: string, without spaces.
"""

version = "1.00"

fullVersion = version  + "dev-augerdev-20200604"

if __name__ == "__main__":
    import sys
    if len(sys.argv) == 2 and sys.argv[1] == "--short":
        print version
    else:
        print fullVersion
